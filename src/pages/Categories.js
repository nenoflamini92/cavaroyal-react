import ItemListContainer from "../components/ItemListContainer/ItemListContainer";
import CategoryList from "../components/CategoryList/CategoryList";
import Hero from "../components/Hero/Hero";

const Categories = () => {
  return (
    <>
    <Hero />
    <main className="site-page section">
      <section className="container">
        <CategoryList />
        <hr className="line" />
        <ItemListContainer sectionTitle="Productos de la categoria" />
      </section>
    </main>
    </>
  );
};

export default Categories;
