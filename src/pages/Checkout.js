import "./Checkout.scss";
import { Link } from "react-router-dom";
import { useContext, useState } from "react";
import { CartContext } from "../context/CartContext";
import Cart from "../components/Cart/Cart";
import { addDoc, collection, getFirestore } from "firebase/firestore";
import swal from 'sweetalert';

const Checkout = () => {
  const { cartProducts, totalPrice } = useContext(CartContext);

  const [success, setSuccess] = useState();

  const [order, setOrder] = useState({
    items: cartProducts.map((product) => {
      return {
        id: product.id,
        title: product.title,
        price: product.price,
      };
    }),
    buyer: {},
    total: totalPrice,
  });

  const [formData, setFormData] = useState({
    name: "",
    last_name: "",
    email: "",
    country: "",
    postal_code: "",
    phone: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const pushFormData = async (newOrder) => {
    const db = getFirestore();
    const orderCollection = collection(db, "orders");
    const orderSnapshot = await addDoc(orderCollection, newOrder);
    setSuccess(orderSnapshot.id);
    console.log("orden cargada:", orderSnapshot);
  };

  const submitUserInfo = (e) => {
    e.preventDefault();
    pushFormData({ ...order, buyer: formData });
  };
  const sweetMessage=() => {
    swal({
      title: "Su orden fue creada con exito!",
      icon:'success',
      button: "Aceptar"
    })
  }
  return (
    <main className="site-page section">
      <section className="container">
        <div className="checkout">
          <form className="checkout-form" onSubmit={submitUserInfo}>
            <p className="checkout-form__title">
              <span>1</span>
              Datos personal
            </p>
            <div className="checkout-form__info">
              <div className="checkout-form__group">
                <div>
                  <label htmlFor="first_name">Nombre</label>
                  <input
                    type="text"
                    id="first_name"
                    name="first_name"
                    required={true}
                    placeholder="Ingrese su nombre"
                    value={formData.first_name}
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <label htmlFor="second_name">Apellido</label>
                  <input
                    type="text"
                    id="last_name"
                    name="last_name"
                    required={true}
                    placeholder="Ingrese su apellido"
                    value={formData.last_name}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div>
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  required={true}
                  name="email"
                  id="email"
                  placeholder="E-mail"
                  value={formData.email}
                  onChange={handleChange}
                />
              </div>
              <div className="checkout-form__group">
                <div>
                  <label htmlFor="country">Pais</label>
                  <input
                    type="text"
                    name="country"
                    id="country"
                    required={true}
                    placeholder="Pais"
                    value={formData.country}
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <label htmlFor="postal_code">Codigo postal</label>
                  <input
                    type="text"
                    required={true}
                    id="postal_code"
                    name="postal_code"
                    placeholder="Ingrese el codigo postal"
                    value={formData.postal_code}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div>
                <label htmlFor="phone">Telefono</label>
                <input
                  type="tel"
                  name="phone"
                  id="phone"
                  required={true}
                  placeholder="Ingrese su telefono"
                  value={formData.phone}
                  onChange={handleChange}
                />
              </div>
            </div>
            <p className="checkout-form__title">
              <span>2</span>
              Información de pago
            </p>
            <div className="checkout-form__payments">
              <div>
                <label htmlFor="credit_card">Tarjeta de credito</label>
                <input
                  type="text"
                  id="credit_card"
                  name="credit_card"
                  placeholder="0000 - 0000 - 0000 - 0000"
                />
              </div>
              <div className="checkout-form__group">
                <div>
                  <label htmlFor="security_code">Codigo de seguridad</label>
                  <input
                    type="text"
                    id="security_code"
                    name="security_code"
                    placeholder="Ingrese el codigo de seguridad"
                  />
                </div>
                <div>
                  <label htmlFor="expiration_date">Fecha de vencimiento</label>
                  <input
                    type="text"
                    name="expiration_date"
                    id="expiration_date"
                    placeholder="00 / 00 / 0000"
                  />
                </div>
              </div> 
              
              <button type="submit" className="checkout-form__payments--btn" onClick={()=>sweetMessage()}>
                Finalizar compra
              </button>
              
            </div>
          </form>
          <div className="checkout-order">
            {cartProducts.length > 0 ? (
              <Cart />
            ) : (
              <div className="empty-cart">
                <p>Tu carrito está vacío</p>
                <div>
                  <span>¿No sabés qué comprar?</span>
                  <Link to="/productos">Encontrá tus productos</Link>
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
      {console.log(order)}
    </main>
  );
};

export default Checkout;
