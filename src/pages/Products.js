import ItemListContainer from "../components/ItemListContainer/ItemListContainer";
import Hero from "../components/Hero/Hero";

const Products = () => {
  return (
    <>
    <Hero />
    <main className="site-page section">
      <section className="container">
        
        <hr className="line" />
        <ItemListContainer />
      </section>
    </main>
    </>
  );
};

export default Products;
