import "./LeftMenu.scss";

const LeftMenu = () => {
  return (
    <>
      <div className="overlay"></div>
      <div className="left-menu">
        <div className="container"></div>
      </div>
    </>
  );
};

export default LeftMenu;
