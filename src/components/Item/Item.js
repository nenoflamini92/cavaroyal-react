import "./Item.scss";
import { Link } from "react-router-dom";
import { changePriceFormat } from "../../helpers";

const Item = ({ item }) => {
  return (
    <Link to={"/item/" + item.id}>
      <article className="product-card line">
        <div className="product-card__image">
          <img src={item.image} alt={item.title} />
        </div>
        <div className="product-card__content">
          <h3>{item.title}</h3>
          <span>{changePriceFormat(item.price)}</span>
          <p>
            <span>en {item.amountOfFees}x sin interés</span>
            de {changePriceFormat(Math.round(item.price / item.amountOfFees))}
          </p>
        </div>
      </article>
    </Link>
  );
};

export default Item;
