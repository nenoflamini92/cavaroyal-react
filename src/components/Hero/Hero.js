import './Hero.scss'
import Card from "../Card/Card";


const Hero = () => {
    const image1 = require("./images/vinos2.jpg");
    const image2 = require("./images/espumantes.jpg");
    const image3 = require("./images/cervezas.jpg");
    const image4 = require("./images/destilados.jpg");

    return (
        <div style={{display: "flex", height: "43vh"}}>
      <Card imagen={image1} titulo={"Vinos"} category={"Vinos"} />
      <Card imagen={image2} titulo={"Espumantes"} category={"Espumantes"} />
      <Card imagen={image3} titulo={"Cervezas"} category={"Cervezas"} />
      <Card imagen={image4} titulo={"Destilados"} category={"Destilados"} />  
      </div>
    );
}

export default Hero;