import "./ItemDetail.scss";
import { useState } from "react";
import { Link } from "react-router-dom";
import ItemCount from "../ItemCount/ItemCount";
import { changePriceFormat } from "../../helpers";

const ItemDetail = ({ item }) => {
  const [quantitySelected, setQuantitySelected] = useState(0);

  return (
    <>
      <article className="item-detail">
        <div className="item-detail__image">
          <img src={item.image} alt={item.title} />
          <Link to={`/category/${item.category}`}>
            <span className="item-detail__image--category">{item.category}</span>
          </Link>
          <div className="item-detail__image--description">
            <h3>Descripción</h3>
            <p>{item.description}</p>
          </div>
          <div className="item-detail__image--share">
            <h3>Compartir</h3>
            <ul>
              <a href="" target="_blank">
                <li>
                  <i className="fa-brands fa-facebook"></i>
                </li>
              </a>
              <a href="" target="_blank">
                <li>
                  <i className="fa-brands fa-instagram"></i>
                </li>
              </a>
              <a href="" target="_blank">
                <li>
                  <i className="fa-brands fa-twitter"></i>
                </li>
              </a>
            </ul>
          </div>
        </div>
        <div className="item-detail__info">
          <h2 className="item-detail__info--title">{item.title}</h2>
          <span className="item-detail__info--price">
            {item.price && changePriceFormat(item.price)}
          </span>
          <div className="item-detail__info--payments">
            <p>
              <span>
                <i className="fa-solid fa-credit-card"></i> {item.amountOfFees}{" "}
                cuotas sin interés
              </span>
              de {changePriceFormat(Math.round(item.price / item.amountOfFees))}
            </p>
            <p>
              <span>
                <i className="fa-solid fa-money-bill-1-wave"></i> 10% de
                descuento
              </span>
              pagando en efectivo o con transferencia
            </p>
            <a href="">Ver más detalles</a>
          </div>
          {quantitySelected > 0 ? (
            <>
              
                <Link to="/cart"><button className="btn1">Terminar compra</button></Link>
              
              
                <Link to="/productos"><button className="btn1">Seguir comprando</button></Link>
              
            </>
          ) : (
            <ItemCount
              stock={item.stock}
              initial={1}
              quantitySelected={setQuantitySelected}
              productData={item}
            />
          )}
          <div className="item-detail__info--shipping">
            <hr className="line" />
            <p>
              <span>
                <i className="fa-solid fa-truck"></i> Envío gratis
              </span>
              superando los $6.000
            </p>
            <div>
              <input type="text" placeholder="Tu código postal" />
              <button>Calcular</button>
            </div>
            <a href="" target="_blank">
              No sé mi código
            </a>
          </div>
        </div>
      </article>
    </>
  );
};

export default ItemDetail;
