import "./ItemDetailContainer.scss";
import React, { useEffect, useState } from 'react'
import { useParams } from "react-router-dom";
import ItemDetail from "../ItemDetail/ItemDetail";
import { doc, getDoc, getFirestore } from "firebase/firestore"

export default function ItemDetailContainer() {

  const [item, setItem] = useState({});
  const { idItem } = useParams();

  useEffect(() => {
    const db = getFirestore();
    const refADoc = doc(db, "items", idItem)

    getDoc(refADoc).then((item) => {
    const aux = {...item.data(), id: item.id};
    setItem(aux)
    });
    }, [idItem]);



    return (
      <section className="item-detail-container">
        <ItemDetail item={item} />
      </section>
    );
}

