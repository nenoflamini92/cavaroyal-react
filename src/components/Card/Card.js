import React from 'react'
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import './Card.scss'

function BasicExample({titulo,imagen,descripcion,category}) {
  return (
    <Link to={`/category/${category}`} key={category}>
    <Card style={{ width: '16.4rem', background: '#440f16b8', margin: 3}} className="card">
      <Card.Img variant="top" src={imagen} style={{width: '16.4rem', height: '10rem'}}/>
      <Card.Body>
        <Card.Title style={{color: 'white', fontSize: '25px', textAlign: 'center'}}>{titulo}</Card.Title>
        <Card.Text style={{color: 'white', fontSize: '18px',  textAlign: 'center'}}>{descripcion}</Card.Text>
      </Card.Body>
    </Card>
    </Link>
  );
}

export default BasicExample;