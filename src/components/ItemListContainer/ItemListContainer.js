import "./ItemListContainer.scss";
import ItemList from "../ItemList/ItemList";
import React, { useEffect, useState } from 'react'
import { collection, getDocs, getFirestore, query, where } from "firebase/firestore"
import { useParams } from 'react-router-dom';

export default function ItemListContainer() {

  const [items, setItems] = useState([]);
  const { category } = useParams();

  useEffect(() => {
    const db = getFirestore();
    let itemCollection;


    if (category == undefined) {
      itemCollection = collection(db, 'items')

    }else {
    itemCollection = query(collection(db, 'items'),
    where("category", "==", category));
    }
    getDocs(itemCollection).then((data) => {
    const products = data.docs.map(item => ({
    ...item.data(), 
    id: item.id
    }));
    setItems(products)
    console.log(products);
    });
    }, [category]);



// const ItemListContainer = ({ sectionTitle }) => {
//   const [listProducts, setListProducts] = useState([]);

//   const { categoryId } = useParams();

//   const filterByCategoryId = products.filter(
//     (product) => product.categoryId == categoryId
//   );

//   const getProducts = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (categoryId) {
//         if (filterByCategoryId.length > 0) {
//           resolve(filterByCategoryId);
//         } else {
//           resolve([]);
//         }
//       } else {
//         resolve(products);
//       }
//     });
//   });

//   useEffect(() => {
//     getProducts
//       .then((res) => setListProducts(res))
//       .catch((err) => console.log(err));
//   }, [categoryId]);

  return (
    <section className="item-list-container">
      <h2 className="item-list-container__title">
        {/* {sectionTitle ? `${sectionTitle}` : "Productos"} */}
        Productos
      </h2>
      <div className="item-list-container__grid">
          <ItemList items={items} />

      </div>
    </section>
  );
};

