import "./Footer.scss";
import Image from 'react-bootstrap/Image'
import { Link } from "react-router-dom";

const Footer = () => {
  
  const imagenfooter = require("../Footer/navbar-logo.png");
  return (
  <div>
    <footer className='text-white py-4'>
        <div className="container">
            <nav className="row">
              <Link to="/" className="col-12 col-md3 d-flex aling-items-center justify-content-center">
              <Image src={imagenfooter} style={{width: '130px', height: '120px', marginLeft: '45%', marginTop: '30px'}}/>
              </Link>
            </nav>
        </div>
    </footer>
</div>
  );
};

export default Footer;
