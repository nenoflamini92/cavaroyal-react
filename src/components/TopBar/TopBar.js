import "./TopBar.scss";

const TopBar = () => {
  return (
    <div className="top-bar">
      <p>Envíos a todo el país | Delivery en Salta Capital</p>
    </div>
  );
};

export default TopBar;
