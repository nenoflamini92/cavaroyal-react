
const products = [
  {
    id: 1,
    title: "RUTINI CABERNET MALBEC",
    price: 2500,
    categoryId: 1,
    category: "Vinos",
    description:
      "Rojo intenso, con matices azulados. En nariz, se presenta frutado, con notas de ciruela, vainilla y anís; mientras, en boca, se reafirman los acentos aciruelados. Los taninos, muy presentes pero amables, destacan su personalidad. Granate intenso con matices purpura, brillante. Impacta su caracter frutado (frambuesa, frutilla, mora), combinado con frescos acentos mentolados, especias y ahumados, que provienen de su cuidada crianza en barrica. Firme en paladar, es redondo, carnoso y fluido. El final de boca brinda una elegante sequedad",
    thumbnail:
    "https://www.puroescabio.com.ar/web/image/product.product/61110/image_1024/%5B5027%5D%20RUTINI%20CABERNET%20FRANC%20MALBEC%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 2,
    title: "NORTON COLONIA MALBEC",
    price: 3660,
    categoryId: 1,
    category: "Vinos",
    description:
      "Color Rojo intenso con aromas Dulce, compleja, con notas a frambuesa y frutos rojos maduros dejando un sabor en boca Condice con la nariz, frutas rojas maduras entre las que destaca la ciruela.",
    thumbnail:
      "https://www.puroescabio.com.ar/web/image/product.product/46016/image_1024/%5B1568%5D%20NORTON%20LOTE%20LA%20COLONIA%20MALBEC%20750ml?unique=7dd3b5c",  
      stock: 10,
      amountOfFees: 3
    },
  
  {
    id: 3,
    title: "LOS NOBLES MALBEC VERDOT",
    price: 5980,
    categoryId: 1,
    category: "Vinos",
    description:
      "Luigi Bosca Finca Los Nobles Field Blend · Malbec Verdot 2013 es un tinto de color púrpura profundo, de aromas equilibrados que recuerdan a frutos negros, moras, grosellas y ciruelas maduras, junto con notas de cassis y algo de café, producto de su crianza en barricas de roble. De a poco el vino se va abriendo y se hace aún más complejo. En la boca se expresa la fruta y ciertas especias en un marco de gran estructura y cuerpo. Sus taninos firmes y maduros le suman fuerza y carácter, en tanto su volumen aporta redondez y textura sedosa. Irá creciendo con el tiempo en botella.",
    thumbnail:
      "https://www.puroescabio.com.ar/web/image/product.product/59550/image_1024/%5B4207%5D%20FINCA%20LOS%20NOBLES%20MALBEC%20VERDOT%20750ml?unique=7dd3b5c",  
      stock: 10,
      amountOfFees: 3
    },
  {
    id: 4,
    title: "LUIGI BOSCA ICONO",
    price: 15130,
    categoryId: 1,
    category: "Vinos",
    description:
    "Luigi Bosca Icono es un tinto de color rojo granate con tintes negros profundos. En nariz es amplio, mostrando arándanos, cerezas y notas florales. Luego aparecen especias como el clavo de olor, anís, vainilla y un agradable dejo de hojas de tabaco maduro. Al paladar es carnoso, amplio y tánico.",
    thumbnail:
      "https://www.puroescabio.com.ar/web/image/product.product/46058/image_1024/%5B1610%5D%20LUIGI%20BOSCA%20ICONO%20750ml?unique=7dd3b5c",  
      stock: 10,
      amountOfFees: 3
    },{
    id: 5,
    title: "DIEGO ROSSO MALBEC",
    price: 3280,
    categoryId: 1,
    category: "Vinos",
    description:
      "Diego Rosso, uno de los pilares de la premiada bodega Achaval Ferrer, ha desarrollado sus propios y exclusivos vinos Pinot Noir y Malbec. Ambos varietales son producidos con uvas provenientes de su viñedo en San Pablo, Tupungato, en un micro-terroir único a 1500 metros sobre el nivel del mar, donde el clima es ideal para preservar los sabores primarios y una acidez natural de la uva. Aplicando sus amplios conocimientos en enología, logra elaborar y ofrecer al mercado expresivos vinos sin filtrar, en muy limitadas producciones.",
    thumbnail:
      "https://www.puroescabio.com.ar/web/image/product.product/48215/image_1024/%5B2514%5D%20DIEGO%20ROSSO%20MALBEC%20750ml?unique=7dd3b5c",  
      stock: 10,
      amountOfFees: 3
    },{
    id: 6,
    title: "GRAN ALMA NEGRA BLEND",
    price: 8290,
    categoryId: 1,
    category: "Vinos",
    description:
      "De cuerpo medio y muy terso, suave de gran equilibrio armónico entre sus frutas. Un tinto espectacular, delicado, elegante y un gran seductor Bodega Ernesto Catena",
    thumbnail:
      "https://www.puroescabio.com.ar/web/image/product.product/52660/image_1024/%5B2995%5D%20GRAN%20ALMA%20NEGRA%20BLEND%201500ml?unique=7dd3b5c",  
      stock: 10,
      amountOfFees: 3
    },
    
  {
    id: 7,
    title: "JOHNNIE WALKER GOLD",
    price: 25930,
    categoryId: 2,
    category: "Destilados",
    description: "Un whisky que celebra el arte de nuestro Master Blender, resultando en un exclusivo blend. Johnnie Walker Gold Label Reserve contiene el prestigioso whisky de malta Clynelish Single Malt Scotch Whisky.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/45555/image_1024/%5B1412%5D%20JOHNNIE%20WALKER%20GOLD%2018%20A%C3%91OS%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 8,
    title: "GIN NORDES",
    price: 16150,
    categoryId: 2,
    category: "Destilados",
    description: "Nordés es una ginebra gallega, inequívocamente. Un gin que sabe a verde, a montaña, a rocas, a un mar enfurecido; incluso a una forma de ser y a una forma de vivir. De sentir. Evocaciones y morriña. Mucho más que un destilado. Uva albariña cultivada tradicionalmente como base, una combinación de botánicos silvestres gallegos, desde hierbaluisa o caralleira a la planta marina salicornia, y Nordés, que así fue bautizada, se convirtió en una tangible realidad.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/45167/image_1024/%5B1181%5D%20GIN%20NORDES%20700ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 9,
    title: "CHIVAS REGAL ULTIS",
    price: 43520,
    categoryId: 2,
    category: "Destilados",
    description: "Whisky escocés de malta que contiene cinco preciosas maltas únicas. Cada uno, seleccionada de cinco destilerías Speyside en honor de las cinco generaciones de Chivas Regal Master Blenders.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/44324/image_1024/%5B580%5D%20CHIVAS%20REGAL%20ULTIS%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 10,
    title: "KETEL ONE VODKA",
    price: 4320,
    categoryId: 2,
    category: "Destilados",
    description: "El nombre Ketel One deriva de Distilleerketel, Numero. 1, una palabra holandesa que significa alambique. La destileria fue fundada en 1691 por la familia Nolet que hoy, luego de 10 generaciones, continúa aprobando cada producción antes de su embotellado. Ketel One se produce en pequeños lotes utilizando alambiques de cobre y modernas técnicas de destilación.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/45655/image_1024/%5B1458%5D%20KETEL%20ONE%20VODKA%20750ml?unique=1d95795",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 11,
    title: "RON BARCELO PREMIUM",
    price: 22130,
    categoryId: 2,
    category: "Destilados",
    description: "Este ron es el resultado del doble envejecimiento de las reservas antiguas del Imperial, reposadas en barricas de roble blanco americano y barricas de roble francés con distintos grados de tueste. Este tipo de ron envolvente, tiene aromas a frutos secos y vanilla prinicipalmente.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/46446/image_1024/%5B1998%5D%20RON%20BARCELO%20PREMIUM%20BLEND%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 12,
    title: "PATRON SILVER",
    price: 16670,
    categoryId: 2,
    category: "Destilados",
    description: "Tequila 100% agave que resalta los sabores mas puros del agave, llamada La Bebida Blanca elaborada con agave azul Tequilana Weber. Tiene aromas a frutas y citricos con un sabor suave y dulce, y un acabado ligero a pimienta.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/46331/image_1024/%5B1883%5D%20PATRON%20SILVER%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 13,
    title: "MOET CHANDON",
    price: 28520,
    categoryId: 3,
    category: "Espumantes",
    description: "Moët Chandon es la marca de champagne más consumida en Francia y en el mundo. Es también propietaria de un viñedo de 500 hectáreas en producción y, al mismo tiempo, de un nombre mítico del Champagne, el del monje.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/46141/image_1024/%5B1693%5D%20MOET%20CHANDON%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 14,
    title: "BARON B EXTRA BRUT",
    price: 4260,
    categoryId: 3,
    category: "Espumantes",
    description: "Se destaca por la delicada espuma y la fineza de sus burbujas. Un sutil color salmón expresa la autenticidad y elegancia de nuestro espumante. En nariz, se destacan atomas intensos y refinados de frutos rojos, en combinación con cítricos y durazno.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/59696/image_1024/%5B4597%5D%20BARON%20B%20EXTRA%20BRUT%20ROSE%20750ml?unique=7eaf8a6",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 15,
    title: "RUTINI EXTRA BRUT",
    price: 3930,
    categoryId: 3,
    category: "Espumantes",
    description: "Amarillo brillante, con reflejos acerados. Presenta cordones de burbujas finas y permanentes.En sus aromas predominan las frutas tropicales frescas combinadas con dejos a pan tostado y frutos secos, como la almendra.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/47608/image_1024/%5B2049%5D%20RUTINI%20EXTRA%20BRUT%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 16,
    title: "ALMA NEGRA BLANC",
    price: 3560,
    categoryId: 3,
    category: "Espumates",
    description: "Color dorado con reflejos acerados, con aroma Intenso y Aromático, frutal y con notas a tostado, Sabor Envolvente, con una acidez ligera que le brinda frescura, con un marinaje de frutos del mar, ostras, langostas, carnes blancas",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/52227/image_1024/%5B2777%5D%20ALMA%20NEGRA%20BLANC%20DE%20BLANCS%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 17,
    title: "NIETO SENETINER MELLÉSIME",
    price: 8920,
    categoryId: 3,
    category: "Espumantes",
    description: "Este excepcional espumoso, elaborado en años especiales por Nieto Senetiner, es realizado con uvas Pinot Noir y Chardonnay. El mismo fue conservado cuidadosamente sobre borras finas que portan a la nariz una delicada nota de pan tostado, praliné y cítricos. En boca una acidez equilibrada, que le confiere persistencia y elegancia.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/52750/image_1024/%5B2879%5D%20NIETO%20SENETINER%20MELL%C3%89SIME%20BRUT%20NATURE%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
  {
    id: 18,
    title: "CASA BOHER EXTRA BRUT",
    price: 3460,
    categoryId: 3,
    category: "Espumantes",
    description: "Presenta un color amarillo pálido, con tonalidades asalmonadas; espuma persistente y burbujas delicadas, muy pequeñas. Encontramos aromas a pan tostado, duraznos, cítricos, manzana, ananá, coco y ciruelas. Buena acidez. Pleno en boca, de mucho cuerpo, largo y sin amargos.",
    thumbnail: "https://www.puroescabio.com.ar/web/image/product.product/44235/image_1024/%5B546%5D%20CASA%20BOHER%20EXTRA%20BRUT%20750ml?unique=7dd3b5c",  
    stock: 10,
    amountOfFees: 3
  },
];

export default products;
