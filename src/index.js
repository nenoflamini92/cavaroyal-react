import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDESYXTU9cLjlBewJPFYWg0SPX3kr2AKSo",
  authDomain: "cava-royal.firebaseapp.com",
  projectId: "cava-royal",
  storageBucket: "cava-royal.appspot.com",
  messagingSenderId: "798984886147",
  appId: "1:798984886147:web:9190c6252281e431fb88b6"
};

initializeApp(firebaseConfig);

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    // <React.StrictMode>
        <App />
    // </React.StrictMode>
);
